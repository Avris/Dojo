<?php
namespace Avris\Dojo;

use Hoa\Event\Bucket;
use Hoa\Socket\Client;
use Hoa\Websocket\Client as WebClient;
use Symfony\Component\Process\Process;

class PullListener
{
    /** @var WebClient */
    private $client;

    public function __construct(string $server)
    {
        $repo = $this->recogniseOrigin();

        $this->client = new WebClient(new Client(rtrim($server, '/') . '/' . base64_encode($repo)));
        $this->client->setHost('localhost');
        $this->client->on('message', function (Bucket $bucket) {
            echo 'Pulling' . PHP_EOL;
            echo $this->git('pull origin master');
        });

        echo sprintf('Listening to %s for pushes to repo %s', $server, $repo) . PHP_EOL;
    }

    public function listen()
    {
        $this->client->run();
    }

    private function recogniseOrigin(): string
    {
        $origin = $this->git('remote -v show');
        if (!preg_match('#^origin\s+(.*)\s+\\(fetch\\)\s*#', $origin, $matches)) {
            throw new \RuntimeException('Cannot determine the URL of remote origin');
        }

        return $matches[1];
    }

    private function git(string $command): string
    {
        $process = new Process('git ' . $command);
        $process->mustRun();

        return $process->getOutput();
    }
}
