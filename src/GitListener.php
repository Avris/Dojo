<?php
namespace Avris\Dojo;

use PHPUnit\Framework\TestListener;
use PHPUnit\Framework\TestListenerDefaultImplementation;
use PHPUnit\Framework\TestSuite;
use Symfony\Component\Process\Process;

class GitListener implements TestListener
{
    use TestListenerDefaultImplementation;

    private static $alreadyRun = false;

    public function startTestSuite(TestSuite $suite): void
    {
        if (static::$alreadyRun) {
            return;
        }

        static::$alreadyRun = true;

        $status = $this->git('status');
        $needsCommit = strpos($status, 'nothing to commit, working tree clean') === false;
        $needsPush = strpos($status, 'Your branch is up to date') === false;

        if (!$needsCommit && !$needsPush) {
            echo 'nothing to commit' . PHP_EOL;
            return;
        }

        if ($needsCommit) {
            $this->commit();
        }

        $this->push();
    }

    private function commit()
    {
        $lastMessage = $this->git('log -1 --pretty=%B');

        $newStep = preg_match('#^Step (\d+)$#', trim($lastMessage), $matches)
            ? $matches[1] + 1
            : 0;

        $this->git('add .');
        $this->git(sprintf('commit -m "Step %s"', $newStep));
        echo sprintf('Step %s committed', $newStep) . PHP_EOL;
    }

    private function push(): void
    {
        $process = $this->gitAsync('push');
        register_shutdown_function(function () use ($process) {
            echo 'Pushing...' . PHP_EOL;
            $process->wait(function (string $type, string $buffer) {
                echo $buffer;
            });
        });
    }

    private function git(string $command): string
    {
        $process = new Process('git ' . $command);
        $process->mustRun();

        return $process->getOutput();
    }

    private function gitAsync(string $command): Process
    {
        $process = new Process('git ' . $command);
        $process->start();

        return $process;
    }
}
